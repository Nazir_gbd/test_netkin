import { Component, Prop } from "nuxt-property-decorator";
import Vue from "vue";
import { VBtn, VSelect, VTextarea, VTextField, VIcon,  } from "vuetify/lib";


export type TCE = typeof Vue.prototype.$createElement;

@Component({
    name: "ToDoList",
})
export class ToDoList extends Vue {

    @Prop()
    public anyProp: string = "";


    public rendAddVbtn(h: TCE) {
        const vbtn = h(
            VBtn, {
            style: ""
        },
            "+ ADD NEW TO DO "
        )
        return vbtn;
    }
    public renderVselect(h: TCE) {
        const vslect = h(
            VSelect,[
                h('item 1'),
                h('item 2'),
                h('item 3'),
              ]
        )
        return vslect;
    }



    public renderVTextField(h: TCE) {
        const vtextfield = h(
            VTextField, {
            attrs: {
                style: "border: solid 2px gray; border-radius: 5px;",
                placeholder:"Title",


            }
            

        }
        )
        return vtextfield;
    }

    public renderFirstRow(h: TCE) {
        const firstrow = h(
            "v-row", { style: "margin:0px" },
            [h("v-col",
                [this.renderVselect(h)]),
            h("v-col",
                { class: "col-6", },
                [this.renderVTextField(h)])]
        )
        return firstrow;
    }

    public renderVTextarea(h: TCE) {
        const vtextarea = h(
            VTextarea, {
            style: "padding:4px",
            attrs: {
                style: "border: solid 2px gray; border-radius: 5px;",
                placeholder:"Description",


            }

        }
        )
        return vtextarea;
    }


    public rendDelVbtn(h: TCE) {
        const vbtn = h(
            VBtn, {
            style: "background:rgb(212, 60, 0); display: flex; align-items: center;"
        },
            [this.rendIcone(h), " DELETE"]
        )
        return vbtn;
    }

    public rendIcone(h: TCE) {
        const vicone = h(
            VIcon, "mdi-cancel",
        )
        return vicone;
    }



    public render(h: TCE) {
        const todolist = h(
            "todolist", {
            attrs: {
                id: "todolist"
            }
        },
            [h("div",
                { style: "display:flex;justify-content: center;padding:1%" },
                [this.rendAddVbtn(h)]),
            h("v-card",
                { style: "width:90%; margin-top:1%;padding:10px; border-radius: 5px; left: 50%;transform: translateX(-50%)" },
                [, h("div",
                    [this.renderFirstRow(h)]),
                    h("div",
                        [this.renderVTextarea(h)],),
                    h("div",
                        { style: "display:flex;justify-content: end;padding:1%;" },
                        [this.rendDelVbtn(h)],)])]
        )

        return todolist;


    }

}

